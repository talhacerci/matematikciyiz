﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Form1.Master" CodeBehind="Hakkimizda.aspx.cs" Inherits="Matematikciyiz.Hakkimizda" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<form runat="server">

    <div style="text-align:center">
        <span class="fa-4x">Hakkımızda</span>
        <br />
        <br />
        <span style="color:mediumblue">Matematikçiyiz olarak bizler birer aileyiz, arkadaşız.</span> <br />
        <span style="color:black">Merhaba arkadaşlar,<br />
        Eksiklerinizi tamamlamanıza yardımcı olması için konuların kolay anlatımlı videolarını çekip bu siteden sizinle paylaşacağım.
        <br />
        Konu anlatımının öğretici olması için anlatım basit olacak.
        <br />
        Soru çözüm videolarında soruların zorluk seviyesi  yükselecek. 
        <br />
        Eksik konularıda zamanla tamamlıyorum :)<br />Herkese başarılar...
        <br />
        Talha Çerçi
        </span>
        <div class="divider"></div>
        <br />
    </div>

</form>

</asp:Content>
