﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Form1.Master" CodeBehind="SoruCozumleri.aspx.cs" Inherits="Matematikciyiz.SoruCozumleri" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<form runat="server">

    <div style="text-align:center">
        <span class="fa-4x">Soru Çözümleri</span>
        <br />
        <br />
        <br />
    </div>
 <div style="font-size:medium">
 <table style="width:100%; height:100%">
  <tr style="border:0">
   <td align="center">
    <table style="text-align:left" >
        <tr >
            <td >
                <table style="text-align:center">
                 <tr height="375">
                        <div class="list-group">
                            <a style="color:red">Temel Konular</a><br />
                            <a href="#" class="list-group-item">Temel Kavramlar</a> <br />
                            <a href="#" class="list-group-item">İşlemler</a> <br />
                            <a href="#" class="list-group-item">Rasyonel Sayılar</a> <br />
                            <a style="color:red">Matematik 1</a><br />
                            <a href="#" class="list-group-item">Hız Problemleri</a> <br />
                            <a href="#" class="list-group-item">Havuz Problemleri</a> <br />
                            <a href="#" class="list-group-item">Faiz Problemleri</a> <br />
                            <a style="color:red">Matematik 2</a><br />
                            <a href="#" class="list-group-item">Trigonometri</a> <br />
                            <a href="#" class="list-group-item">Türev</a> <br />
                            <a href="#" class="list-group-item">İntegral</a> <br />
                        </div>
                 </tr>
                </table>
            </td>
        </tr>
        </table>
      </td>
      <td align="center">
          <table style="text-align:center">
            <tr>
                <table style="text-align:center">
                    <tr>
                        <article>
                    <img src="images/kayıtolun.jpg" />
                            </article>
            </tr>
           </table>
            </tr>
        </table>
    </td>
  </tr>
 </table>
 </div> 

</form>

</asp:Content>
