﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Form1.Master" CodeBehind="Iletisim.aspx.cs" Inherits="Matematikciyiz.Iletisim" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<form runat="server">

    <div style="text-align:center">
        <span class="fa-4x">İletişim</span>
        <br />
        <br />
        <span style="color:mediumblue">Kafana takılan bir konu varsa Matematikçiyiz olarak bizler senin yanındayız.</span> <br />
        <span style="color:black">Talha Çerci Kitap Evi<br />
        Fatih/İstanbul
        <br />
        Telefon: 0512 212 22 22
        <br />
        Fax: 0512 212 22 23
        <br />
        E-Mail Adresi: talhacercikitapevi@gmail.com
        </span>
        <div class="divider"></div>
        <br />
    </div>

</form>

</asp:Content>
