﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Form1.Master" CodeBehind="Anasayfa.aspx.cs" Inherits="Matematikciyiz.Anasayfa" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

			<section class="banner full">
				<article>
					<img src="images/slayt1.jpg" alt="" />
				</article>
				<article>
					<img src="images/slayt2.jpg" alt="" />
					<div class="inner">
						<header>
							<p>KONU ANLATIMLARIMIZ İLE KOLAYCA ÖĞREN</p>
							<h2>Matematikçiyiz</h2>
						</header>
					</div>
				</article>
				<article>
					<img src="images/slayt3.jpg"  alt="" />
					<div class="inner">
						<header>
							<p>SORU ÇÖZÜMLERİ İLE KENDİNİ GELİŞTİR</p>
							<h2>Matematikçiyiz</h2>
						</header>
					</div>
				</article>
				<article>
					<img src="images/slayt4.jpg"  alt="" />
					<div class="inner">
						<header>
							<p>MATEMAĞİ GÖZÜNDE BÜYÜTME!</p>
							<h2>Matematikçiyiz</h2>
						</header>
					</div>
				</article>
			</section>

			<section id="three" class="wrapper style2">
				<div class="inner">
					<header class="align-center">
						<p class="special">Tüm konu anlatım ve soru çözüm içeriklerine ulaşabilmeniz için OTURUM AÇ!</p>
						<h2>ÖRNEK KONU ANLATIMLARI</h2>
					</header>
					<div class="gallery">
						<div>
							<div class="image fit">
								<a href="#"><img src="images/pic01.jpg" alt="" /></a>
							</div>
						</div>
						<div>
							<div class="image fit">
								<a href="#"><img src="images/pic02.jpg" alt="" /></a>
							</div>
						</div>
						<div>
							<div class="image fit">
								<a href="#"><img src="images/pic03.jpg" alt="" /></a>
							</div>
						</div>
						<div>
							<div class="image fit">
								<a href="#"><img src="images/pic04.jpg" alt="" /></a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="two" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Matematiği kullanmayan bilimler, ele aldıkları konularda ancak dış yapıyı inceleyebilirler; çünkü matematikle dile getirdikleri, ancak birtakım bağıntılardır; bu bağıntılar ise özle ilgili unsurlar arasında değil, dış görünüşle ilgili noktalar arasında olabileceğinden, bir varlığın özünü, onun aslında ne olduğunu bize vermekten acizdirler. O halde matematik, tabiat bilimleri, tarih gibi kişiliğin içlerine nüfuz edip, onu derin bir sezgi ile kavrayabilen bir disiplinin önünde çok aşağı niteliktedirler. </p>
						<h2>M. Kemal Atatürk</h2>
					</header>
				</div>
			</section>

			<section id="one" class="wrapper style2">
				<div class="inner">
					<div class="grid-style">

						<div>
							<div class="box">
                                <table style="width:100%; height:100%">
                                <tr style="border:0; background-color:white">
                                <td align="center">
                                <table style="text-align:left" >
                                <tr >
								<div class="image fa">
									<img src="images/oturumac.png" alt="" />
								</div>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table>
								<div class="content">
									<header class="align-center">
										<p style="color:red">İçeriklere ulaşabilmeniz için oturum açmanız gerekir.</p>
										<h2>OTURUM AÇ</h2>
									</header>
									<p>Sitemiz güvenliği ve prensiplerimiz gereği konu anlatıları ve soru çözümleri içeriklerimizi oturum açmayan kişilere kısıtlıyoruz. Sitemizden tam manasıyla yararlanabilmek ve güvenliğiniz için oturum açmanız yeterli.</p>
									<footer class="align-center">
										<a href="OturumAc.aspx" class="button alt">Oturum Aç</a>
									</footer>
								</div>
							</div>
						</div>

						<div>
							<div class="box">
								<table style="width:100%; height:100%">
                                <tr style="border:0; background-color:white">
                                <td align="center">
                                <table style="text-align:left" >
                                <tr >
								<div class="image fa">
									<img src="images/kayıtol.png" alt="" />
								</div>
                                </tr>
                                </table>
                                </td>
                                </tr>
                                </table>
								<div class="content">
									<header class="align-center">
										<p style="color:red">İçeriklerimize ulaşabilmek için kayıt olmanız gerekmektedir.</p>
										<h2>KAYIT OL</h2>
									</header>
									<p>Sitemiz güvenliği ve prensiplerimiz gereği konu anlatıları ve soru çözümleri içeriklerimizi oturum açmayan kişilere kısıtlıyoruz. Sitemizden tam manasıyla yararlanabilmek ve güvenliğiniz için kayıt olmanız yeterli.</p>
									<footer class="align-center">
										<a href="KayitOl.aspx" class="button alt">Kayıt Ol</a>
									</footer>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>

</asp:Content>
