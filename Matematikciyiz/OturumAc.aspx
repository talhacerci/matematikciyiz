﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Form1.Master" CodeBehind="OturumAc.aspx.cs" Inherits="Matematikciyiz.OturumAc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

<form runat="server">
    <div style="text-align:center">
        <span class="fa-4x">Oturum Aç</span>
        <br />
        <br />
        <span style="color:mediumblue">Tüm Alanların Doldurulması Zorunludur.</span>
        <div class="divider"></div>
        <br />
    </div>
 <div style="font-size:medium">
 <table style="width:100%; height:100%">
  <tr style="border:0">
   <td align="center">
    <table style="text-align:center">
        <tr>
            <td>Kullanıcı Adı:</td>
            <td><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Kullanıcı Şifresi:</td>
            <td><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td></td>
            <td><asp:Button ID="Button1" runat="server" Text="Oturum Aç" /></td>
        </tr>
    </table>
   </td>
  </tr>
 </table>
 </div> 
<div class="divider"></div>
</form>

</asp:Content>
